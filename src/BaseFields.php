<?php

namespace Drupal\lightning_scheduler;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Utility class containing a static function to return base field definitions.
 *
 * @internal
 *   This is an internal part of Lightning Scheduler and may be changed or
 *   removed at any time without warning. It should not be used by external
 *   code in any way.
 */
final class BaseFields {

  /**
   * Returns an array of base fields used by lightning scheduler.
   *
   * @return array
   *   An array of base field definitions, keyed by their field name.
   */
  public static function getLightningSchedulerBaseFieldDefinitions() {
    return [
      'scheduled_transition_date' => BaseFieldDefinition::create('datetime')
        ->setDisplayConfigurable('view', FALSE)
        ->setDisplayConfigurable('form', FALSE)
        ->setLabel(t('Scheduled transition date'))
        ->setTranslatable(TRUE)
        ->setRevisionable(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED),
      'scheduled_transition_state' => BaseFieldDefinition::create('string')
        ->setDisplayConfigurable('view', FALSE)
        ->setDisplayConfigurable('form', FALSE)
        ->setLabel(t('Scheduled transition state'))
        ->setTranslatable(TRUE)
        ->setRevisionable(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED),
    ];
  }

}
