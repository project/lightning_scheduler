<?php

/**
 * @file
 * Contains lightning scheduler post update hooks.
 */

use Drupal\lightning_scheduler\BaseFields;

/**
 * Update existing entity types that can be moderated but are not.
 */
function lightning_scheduler_post_update_remove_base_fields() {
  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_definition_update_manager = \Drupal::entityDefinitionUpdateManager();

  /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_information */
  $moderation_information = \Drupal::service('content_moderation.moderation_information');

  foreach ($entity_type_manager->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$moderation_information->isModeratedEntityType($entity_type)) {
      foreach (array_keys(BaseFields::getLightningSchedulerBaseFieldDefinitions()) as $field_name) {
        if ($field_storage_definition = $entity_definition_update_manager->getFieldStorageDefinition($field_name, $entity_type_id)) {
          $entity_definition_update_manager->uninstallFieldStorageDefinition($field_storage_definition);
        }
      }
    }
  }
}
