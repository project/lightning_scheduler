<?php

namespace Drupal\lightning_scheduler_test;

use Drupal\Component\Datetime\Time as BaseTime;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @internal
 *   This is an internal part of Lightning Scheduler and may be changed or
 *   removed at any time without warning. It should not be used by external
 *   code in any way.
 */
class Time extends BaseTime {

  /**
   * {@inheritdoc}
   */
  public function getRequestTime() {
    return $this->getCurrentTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTime() {
    return \Drupal::state()->get('lightning_scheduler.request_time', parent::getRequestTime());
  }

}
