<?php

namespace Drupal\Tests\lightning_scheduler\Functional;

use Drupal\lightning_scheduler_test\Time;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\lightning_scheduler\Traits\SchedulerUiTrait;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
/**
 * @group lightning_scheduler
 *
 * @requires inline_entity_form
 */
class InlineEntityFormTest extends BrowserTestBase {

  use EntityReferenceFieldCreationTrait;
  use SchedulerUiTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'inline_entity_form',
    'lightning_scheduler',
    'lightning_scheduler_test',
    'node',
  ];

  protected $currentTime = 1625097600;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setRequestTime($this->currentTime);

    $this->createContentType(['type' => 'alpha']);
    $this->createContentType(['type' => 'beta']);

    $this->createEntityReferenceField('user', 'user', 'field_inline_entity', 'Inline entity', 'node', 'default', [
      'target_bundles' => [
        'alpha' => 'alpha',
      ],
    ]);
    $this->container->get('entity_display.repository')
      ->getFormDisplay('user', 'user')
      ->setComponent('field_inline_entity', [
        'type' => 'inline_entity_form_simple',
      ])
      ->save();

    $this->createEntityReferenceField('node', 'alpha', 'field_inline_entity', 'Inline entity', 'node', 'default', [
      'target_bundles' => [
        'beta' => 'beta',
      ],
    ]);
    $this->container->get('entity_display.repository')
      ->getFormDisplay('node', 'alpha')
      ->setComponent('field_inline_entity', [
        'type' => 'inline_entity_form_simple',
      ])
      ->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'alpha');
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'beta');
    $workflow->save();

    // Inline Entity Form has a problem referencing entities with other than
    // admin users.
    // @see https://www.drupal.org/project/inline_entity_form/issues/2753553
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Asserts that an inline entity form for field_inline_entity exists.
   *
   * @return \Behat\Mink\Element\NodeElement
   *   The inline entity form element.
   */
  private function assertInlineEntityForm() {
    return $this->assertSession()
      ->elementExists('css', '#edit-field-inline-entity-wrapper');
  }

  public function testHostEntityWithoutModeration() {
    $assert_session = $this->assertSession();

    // Test with an un-moderated host entity.
    $this->drupalGet('/user/' . $this->rootUser->id() . '/edit');
    $assert_session->statusCodeEquals(200);
    $inline_entity_form = $this->assertInlineEntityForm();
    $inline_entity_form->fillField('Title', 'Kaboom?');
    $assert_session->selectExists('field_inline_entity[0][inline_entity_form][moderation_state][0][state]', $inline_entity_form);
    $this->getSession()->getPage()->pressButton('Save');
    $assert_session->statusCodeEquals(200);
  }

  /**
   * @depends testHostEntityWithoutModeration
   */
  public function testHostEntityWithModeration() {
    $page = $this->getSession()->getPage();

    // Test with a moderated host entity.
    $this->drupalGet('node/add/alpha');
    $page->fillField('Title', 'Foobar');
    $this->assertInlineEntityForm()->fillField('Title', 'Foobar');

    $host_field = 'moderation_state[0][scheduled_transitions][data]';
    $inline_field = 'field_inline_entity[0][inline_entity_form][moderation_state][0][scheduled_transitions][data]';

    $transition_1 = [
      [
        'state' => 'published',
        'when' => $this->currentTime + 100,
      ],
    ];
    $transition_2 = [
      [
        'state' => 'published',
        'when' => $this->currentTime + 200,
      ],
    ];
    $this->setTransitionData($host_field, $transition_1);
    $this->setTransitionData($inline_field, $transition_2);
    $page->pressButton('Save');

    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $storage = $this->container->get('entity_type.manager')->getStorage('node');
    $alpha = $storage->loadByProperties(['type' => 'alpha']);
    $beta = $storage->loadByProperties(['type' => 'beta']);
    $this->assertCount(1, $alpha);
    $this->assertCount(1, $beta);

    $this->drupalGet(reset($alpha)->toUrl('edit-form'));
    $this->assertTransitionData($host_field, $transition_1);

    $this->drupalGet(reset($beta)->toUrl('edit-form'));
    $this->assertTransitionData($host_field, $transition_2);
  }

}
