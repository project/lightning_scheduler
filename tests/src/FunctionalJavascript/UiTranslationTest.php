<?php

namespace Drupal\Tests\lightning_scheduler\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\lightning_scheduler\Traits\SchedulerUiTrait;

/**
 * @group lightning_scheduler
 */
class UiTranslationTest extends WebDriverTestBase {

  use SchedulerUiTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'language',
    'lightning_scheduler',
    'lightning_scheduler_test',
    'locale',
    'node',
  ];

  protected $currentTime = 1625097600;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setRequestTime($this->currentTime);

    $this->drupalPlaceBlock('local_tasks_block');
    $this->setUpTimeZone();
    $this->setTimeStep();
    $this->setAllowPastDates();
  }

  public function testUiTranslation() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->createContentType(['type' => 'page']);

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->save();

    $account = $this->createUser([
      'administer languages',
      'create page content',
      'view own unpublished content',
      'edit own page content',
      'use editorial transition create_new_draft',
      'schedule editorial transition publish',
      'schedule editorial transition archive',
    ]);
    $this->drupalLogin($account);

    // Install a new language.
    $language_code = 'hu';
    $this->installLanguage($language_code);

    $this->drupalGet('/node/add/page');
    $page->fillField('Title', $this->randomString());

    $this->createTransition('Published', mktime(18, 0, 0, 5, 4, 2038));
    $this->createTransition('Archived', mktime(8, 57, 0, 9, 19, 2038));

    $page->pressButton('Save');
    $this->drupalGet('/hu/node/1/edit');
    $assert_session->pageTextContains("2038. május 04. 18:00:00");
    $assert_session->pageTextContains("2038. szeptember 19. 08:57:00");
  }

  /**
   * Installs a language.
   *
   * @param string $language_code
   *   The language code to install.
   */
  protected function installLanguage($language_code) {
    $this->drupalGet('/admin/config/regional/language/add');
    $edit = [
      'predefined_langcode' => $language_code,
    ];
    $this->submitForm($edit, 'Add language');
  }

}
