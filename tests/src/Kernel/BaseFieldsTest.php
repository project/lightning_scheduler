<?php

namespace Drupal\Tests\lightning_scheduler\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;

/**
 * @group lightning_scheduler
 */
class BaseFieldsTest extends KernelTestBase {

  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'datetime',
    'lightning_scheduler',
    'lightning_scheduler_test',
    'node',
    'system',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('content_moderation_state');
    $this->installConfig('content_moderation');
  }

  public function testBaseFieldDefinitions() {
    NodeType::create(['type' => 'page'])->save();
    $workflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($workflow, 'node', 'page');

    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions */
    $field_definitions = $this->container->get('entity_field.manager')
      ->getBaseFieldDefinitions('node');

    $this->assertEquals('Scheduled transition date', $field_definitions['scheduled_transition_date']->getLabel());
    $this->assertEquals('Scheduled transition state', $field_definitions['scheduled_transition_state']->getLabel());
  }

}
